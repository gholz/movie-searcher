package com.gholz.silly

import android.app.Application
import com.gholz.silly.di.diModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initDependencyInjection()
    }

    private fun initDependencyInjection() {
        startKoin {
            androidContext(this@MainApplication)
            modules(diModules)
        }
    }
}