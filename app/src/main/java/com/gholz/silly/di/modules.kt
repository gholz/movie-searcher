package com.gholz.silly.di

import androidx.room.Room
import com.gholz.silly.BuildConfig
import com.gholz.silly.db.MoviesDatabase
import com.gholz.silly.domain.MoviesRepository
import com.gholz.silly.feature.home.HomeViewModel
import com.gholz.silly.feature.movie.MovieDetailViewModel
import com.gholz.silly.feature.settings.SettingsViewModel
import com.gholz.silly.network.MoviesApi
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val appModule = module {
    single { MoviesRepository(get(), get()) }
}

val databaseModule = module {
    single {
        Room.databaseBuilder(androidContext(), MoviesDatabase::class.java, "movies.db")
            .build()
    }
}

val networkModule = module {
    single {
        Interceptor {
            val url = it.request().url()
            val request = it.request()
                .newBuilder()
                .url(url.newBuilder()
                    .addQueryParameter("apikey", BuildConfig.API_KEY.toString())
                    .build())
                .build()
            return@Interceptor it.proceed(request)
        }
    }
    single {
        OkHttpClient.Builder()
            .addInterceptor(get())
            .build()
    }
    single {
        Retrofit.Builder()
            .baseUrl("http://www.omdbapi.com/")
            .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
            .addConverterFactory(GsonConverterFactory.create())
            .client(get())
            .build()
            .create(MoviesApi::class.java)
    }
}

val viewModelsModule = module {
    viewModel { HomeViewModel(get()) }
    viewModel { MovieDetailViewModel(get()) }
    viewModel { SettingsViewModel() }
}

val diModules = listOf(appModule, viewModelsModule, databaseModule, networkModule)