package com.gholz.silly.feature.movie

import com.gholz.silly.db.model.Movie
import com.gholz.silly.feature.common.BaseViewState

sealed class MovieDetailViewState(
    val data: Movie? = null,
    val title: String = "",
    val showLoading: Boolean = false,
    val showContent: Boolean = false,
    val showError: Boolean = false,
    val errorMessage: String = ""
) : BaseViewState {
    class Loading : MovieDetailViewState(showLoading = true)
    class Error(message:String) : MovieDetailViewState(showError = true, errorMessage = message)
    class Success(data: Movie) : MovieDetailViewState(
        data = data,
        title = "${data.title} (${data.year})"
    )
}