package com.gholz.silly.feature.movie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.NavigationUI
import com.gholz.silly.databinding.FragmentMovieDetailBinding
import com.gholz.silly.feature.common.BaseDataBindingFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class MovieDetailFragment :
    BaseDataBindingFragment<FragmentMovieDetailBinding, MovieDetailViewModel, MovieDetailViewState>() {

    override val vm: MovieDetailViewModel by viewModel()
    private val args: MovieDetailFragmentArgs by navArgs()

    override fun onCreateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentMovieDetailBinding {
        return FragmentMovieDetailBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        NavigationUI.setupWithNavController(binding.toolbar, findNavController())
    }

    override fun onStart() {
        super.onStart()
        vm.loadMovie(args.movieId)
    }
}