package com.gholz.silly.feature.settings

import android.view.LayoutInflater
import android.view.ViewGroup
import com.gholz.silly.databinding.FragmentSettingsBinding
import com.gholz.silly.feature.common.BaseDataBindingFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class SettingsFragment :
    BaseDataBindingFragment<FragmentSettingsBinding, SettingsViewModel, SettingsViewState>() {

    override val vm: SettingsViewModel by viewModel()

    override fun onCreateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentSettingsBinding {
        return FragmentSettingsBinding.inflate(inflater, container, false)
    }
}