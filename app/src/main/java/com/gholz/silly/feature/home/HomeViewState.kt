package com.gholz.silly.feature.home

import androidx.annotation.DrawableRes
import com.gholz.silly.db.model.Movie
import com.gholz.silly.feature.common.BaseViewState

sealed class HomeViewState(
    val showLoading: Boolean = false,
    val showResults: Boolean = false,
    val showWarning: Boolean = false,
    @DrawableRes
    val warningImage: Int = android.R.color.transparent,
    val warningMessage: String? = null,
    val data: List<Movie>? = null
) : BaseViewState {
    class Loading : HomeViewState(showLoading = true)
    class Error(message: String) : HomeViewState(
        showWarning = true,
        warningMessage = message,
        warningImage = android.R.drawable.ic_dialog_alert
    )
    class Empty(message: String) : HomeViewState(
        showWarning = true,
        warningMessage = message,
        warningImage = android.R.drawable.ic_menu_search
    )
    class Success(data: List<Movie>) : HomeViewState(data = data, showResults = true)
}