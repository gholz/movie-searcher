package com.gholz.silly.feature.common.items

import com.gholz.silly.R
import com.gholz.silly.databinding.ItemMovieBinding
import com.gholz.silly.db.model.Movie
import com.xwray.groupie.databinding.BindableItem

class MovieListItem(val movie: Movie) : BindableItem<ItemMovieBinding>() {

    override fun getLayout(): Int {
        return R.layout.item_movie
    }

    override fun bind(binding: ItemMovieBinding, position: Int) {
        binding.movie = movie
    }
}