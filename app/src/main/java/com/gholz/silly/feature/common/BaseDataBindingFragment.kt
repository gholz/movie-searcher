package com.gholz.silly.feature.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.gholz.silly.BR


abstract class BaseDataBindingFragment<B : ViewDataBinding, V : BaseViewModel<S>, S : BaseViewState> :
    Fragment() {

    protected lateinit var binding: B
    protected abstract val vm: V

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = onCreateBinding(inflater, container)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = this
        binding.setVariable(BR.vm, vm)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        vm.state.observe(this, Observer { it?.let { onStateChanged(it) } })
    }

    protected open fun onStateChanged(state: S) {
        // override for state updates
    }

    abstract fun onCreateBinding(inflater: LayoutInflater, container: ViewGroup?): B
}