package com.gholz.silly.feature.home

import androidx.lifecycle.viewModelScope
import com.gholz.silly.domain.MoviesRepository
import com.gholz.silly.feature.common.BaseViewModel
import kotlinx.coroutines.launch

class HomeViewModel(private val repository: MoviesRepository) : BaseViewModel<HomeViewState>() {

    fun search(query: String = "Highlander") {
        viewModelScope.launch {
            state.postValue(HomeViewState.Loading())
            val result = repository.search(query)
            if (result.success) {
                val data = result.data ?: listOf()
                state.postValue(
                    if (data.isEmpty()) {
                        HomeViewState.Empty("No results found for $query")
                    } else {
                        HomeViewState.Success(data)
                    }
                )
            } else {
                state.postValue(HomeViewState.Error(result.message ?: "Error loading movies"))
            }
        }
    }
}
