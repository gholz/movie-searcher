package com.gholz.silly.feature.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.gholz.silly.databinding.FragmentHomeBinding
import com.gholz.silly.db.model.Movie
import com.gholz.silly.feature.common.BaseDataBindingFragment
import com.gholz.silly.feature.common.items.MovieListItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseDataBindingFragment<FragmentHomeBinding, HomeViewModel, HomeViewState>() {

    private val adapter = GroupAdapter<ViewHolder>()
    override val vm: HomeViewModel by viewModel()

    override fun onCreateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentHomeBinding {
        return FragmentHomeBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
    }

    override fun onStart() {
        super.onStart()
        vm.search()
    }

    override fun onStateChanged(state: HomeViewState) {
        if (state is HomeViewState.Success) {
            refreshAdapter(state.data)
        }
    }

    private fun initAdapter() {
        adapter.apply {
            binding.list.adapter = this
            setOnItemClickListener { item, _ ->
                if (item is MovieListItem) {
                    navigateToDetail(item)
                }
            }
        }
    }

    private fun refreshAdapter(data: List<Movie>?) {
        val items = mutableListOf<MovieListItem>()
        data?.forEach {
            val item = MovieListItem(it)
            items.add(item)
        }
        adapter.clear()
        adapter.addAll(items)
    }

    private fun navigateToDetail(item: MovieListItem) {
        HomeFragmentDirections.actionHomeFragmentToMovieDetailFragment(item.movie.id)
            .let { directions -> findNavController().navigate(directions) }
    }
}