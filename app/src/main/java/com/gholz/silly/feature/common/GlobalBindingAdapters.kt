package com.gholz.silly.feature.common

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.BindingConversion
import com.bumptech.glide.Glide

@BindingConversion
fun convertVisibility(visible: Boolean): Int {
    return if (visible) View.VISIBLE else View.GONE
}

@BindingAdapter(value = ["imageUrl"])
fun setImageUrl(view: ImageView, url:String?) {
    url?.let { Glide.with(view).load(it).centerCrop().into(view) }
}