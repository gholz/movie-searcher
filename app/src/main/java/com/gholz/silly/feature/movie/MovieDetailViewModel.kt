package com.gholz.silly.feature.movie

import androidx.lifecycle.viewModelScope
import com.gholz.silly.domain.MoviesRepository
import com.gholz.silly.feature.common.BaseViewModel
import kotlinx.coroutines.launch

class MovieDetailViewModel(private val repository: MoviesRepository) :
    BaseViewModel<MovieDetailViewState>() {

    fun loadMovie(id: String) {
        viewModelScope.launch {
            state.postValue(MovieDetailViewState.Loading())
            val response = repository.getMovieById(id)
            if (response.success) {
                response.data?.let {
                    state.postValue(MovieDetailViewState.Success(it))
                } ?: run {
                    state.postValue(MovieDetailViewState.Error("Id $id not found"))
                }
            } else {
                state.postValue(MovieDetailViewState.Error("Error loading movie"))
            }
        }
    }
}