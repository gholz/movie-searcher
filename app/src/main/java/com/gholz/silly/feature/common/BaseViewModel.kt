package com.gholz.silly.feature.common

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel<S: BaseViewState> : ViewModel() {

    val state = MutableLiveData<S>()
}