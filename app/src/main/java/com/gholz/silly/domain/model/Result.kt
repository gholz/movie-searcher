package com.gholz.silly.domain.model

class Result<T>(val success: Boolean, val data: T?, val message: String? = null)
