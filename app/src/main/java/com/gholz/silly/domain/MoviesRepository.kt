package com.gholz.silly.domain

import com.gholz.silly.db.MoviesDatabase
import com.gholz.silly.db.model.Movie
import com.gholz.silly.domain.model.Result
import com.gholz.silly.network.MoviesApi

class MoviesRepository(
    private val api: MoviesApi,
    private val database: MoviesDatabase
) {

    suspend fun search(query: String): Result<List<Movie>> {
        val response = api.search(query).await()
        val movies = response.movies.map {
            Movie(
                id = it.imdbID,
                title = it.title,
                year = it.year,
                poster = it.poster
            )
        }
        database.getMoviesDAO().upsert(movies)
        return Result(true, movies)
    }

    suspend fun getMovieById(id: String): Result<Movie> {
        val response = api.getMovieById(id).await()
        val movie = Movie(
            id = response.imdbID,
            actors = response.actors,
            awards = response.awards,
            boxOffice = response.boxOffice,
            country = response.country,
            director = response.director,
            genre = response.genre,
            rating = response.imdbRating,
            language = response.language,
            plot = response.plot,
            poster = response.poster,
            rated = response.rated,
            released = response.released,
            title = response.title,
            writer = response.writer,
            year = response.year
        )
        database.getMoviesDAO().upsert(movie)
        return Result(true, movie)
    }
}