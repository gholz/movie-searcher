package com.gholz.silly.db

import androidx.room.*
import com.gholz.silly.db.model.Movie

@Dao
interface MoviesDAO {

    @Query("SELECT * FROM movies WHERE title LIKE :query")
    suspend fun search(query:String): List<Movie>

    @Query("SELECT * FROM movies WHERE id = :id")
    suspend fun findById(id: String): Movie

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(movie: Movie): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(movies: List<Movie>): List<Long>

    @Update
    suspend fun update(movie: Movie)

    @Update
    suspend fun update(movies: List<Movie>)

    @Transaction
    suspend fun upsert(movie: Movie) {
        val id = insert(movie)
        if (id == -1L) {
            update(movie)
        }
    }

    @Transaction
    suspend fun upsert(movies: List<Movie>) {
        val updateList = mutableListOf<Movie>()
        insert(movies).forEachIndexed { index, id ->
            if (id == -1L) {
                updateList.add(movies[index])
            }
        }
        if (updateList.isNotEmpty()) {
            update(updateList)
        }
    }
}