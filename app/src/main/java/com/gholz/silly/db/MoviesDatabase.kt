package com.gholz.silly.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gholz.silly.db.model.Movie

@Database(entities = [Movie::class], version = 1, exportSchema = false)
abstract class MoviesDatabase : RoomDatabase() {
    abstract fun getMoviesDAO(): MoviesDAO
}