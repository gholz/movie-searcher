package com.gholz.silly.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "movies")
class Movie(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String = "",
    @ColumnInfo(name = "actors")
    val actors: String = "",
    @ColumnInfo(name = "awards")
    val awards: String = "",
    @ColumnInfo(name = "box_office")
    val boxOffice: String = "",
    @ColumnInfo(name = "country")
    val country: String = "",
    @ColumnInfo(name = "director")
    val director: String = "",
    @ColumnInfo(name = "genre")
    val genre: String = "",
    @ColumnInfo(name = "rating")
    val rating: String = "",
    @ColumnInfo(name = "language")
    val language: String = "",
    @ColumnInfo(name = "plot")
    val plot: String = "",
    @ColumnInfo(name = "poster")
    val poster: String = "",
    @ColumnInfo(name = "rated")
    val rated: String = "",
    @ColumnInfo(name = "released")
    val released: String = "",
    @ColumnInfo(name = "title")
    val title: String = "",
    @ColumnInfo(name = "writer")
    val writer: String = "",
    @ColumnInfo(name = "year")
    val year: String = ""
)