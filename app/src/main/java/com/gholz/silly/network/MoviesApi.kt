package com.gholz.silly.network

import com.gholz.silly.network.model.MovieDetail
import com.gholz.silly.network.model.SearchResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesApi {

    @GET("/")
    fun search(@Query("s") query: String = ""): Deferred<SearchResponse>

    @GET("/")
    fun getMovieById(@Query("i") id: String): Deferred<MovieDetail>

}