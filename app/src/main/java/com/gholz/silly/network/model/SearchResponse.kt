package com.gholz.silly.network.model


import com.google.gson.annotations.SerializedName

data class SearchResponse(
    @SerializedName("Search")
    val movies: List<MovieItem> = listOf(),
    @SerializedName("totalResults")
    val total: String = ""
)